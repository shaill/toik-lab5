package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations{
    private List<String> list = new ArrayList<>();

    @Override
    public List<String> get() {
        List<String> stack = new ArrayList<>(this.list);

        this.list.removeAll(stack);

        return stack;
    }

    @Override
    public Optional<String> pop() {
        if(list.isEmpty()) {
            return Optional.empty();
        }
        else{
            String value = this.list.get(this.list.size() - 1);
            this.list.remove(this.list.size() - 1);
            return Optional.of(value);
        }
    }

    @Override
    public void push(String item) {
        list.add(item);
    }
}
